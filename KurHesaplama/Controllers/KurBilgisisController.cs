﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KurHesaplama.Models;
using KurHesaplama.Models.DB;
using System.Xml;
using System.Runtime.Remoting.Contexts;
using System.Globalization;
using System.Diagnostics;
using System.Data.Entity.Validation;

namespace KurHesaplama.Models.ViewModel
{
    public class KurBilgisisController : Controller
    {
        private KurHesaplamaEntities1 db = new KurHesaplamaEntities1();
       
        // GET: KurBilgisis
        [HttpGet]
        public ActionResult Tarih()
        {
            int result = db.Database.ExecuteSqlCommand("TRUNCATE TABLE dbo.KurBilgisi");
            int result2 = db.Database.ExecuteSqlCommand("DBCC CHECKIDENT ('KurHesaplama.dbo.KurBilgisi', RESEED, 0)");
            return View();
        }
        [HttpPost]
        public ActionResult Tarih(DateTime date)
        {
         
            KurBilgisi kur = new KurBilgisi();
            string a= date.ToShortDateString();
            //DateTime date2 = DateTime.ParseExact(a, "dd.MM.yyyy", null);
            // Debug.WriteLine("---------", dt.ToString());
            TCMBDovizKuruAl(kur, date);
            return RedirectToAction("/Index");
        }
        public ActionResult Index()
        {  
            return View(db.KurBilgisi.ToList());
        }


        public ActionResult Hesapla()
        {
            ViewBag.Kod1 = new SelectList(db.KurBilgisi.Select(model => model.Kod).Distinct(), "Kod");
           // ViewBag.Kod2 = new SelectList(db.KurBilgisi.Select(model => model.Kod).Distinct(), "Kod2");
            return View();
        }
        [HttpPost]
        public ActionResult Hesapla(KurBilgileri kurBilgileri)
        {
            ViewBag.Kod1 = new SelectList(db.KurBilgisi.Select(model => model.Kod).Distinct(), "Kod");
            var donen = db.KurBilgisi.Where(b => b.Kod == kurBilgileri.Kod);
            //donen.Where(b => b.Kod==kurBilgileri.Kod).FirstOrDefault();
            System.Diagnostics.Debug.WriteLine("sonucccccccccccccccc", donen);
            return View();
        }

        public void TCMBDovizKuruAl([Bind(Include = "Id,Kod,Cins,Alis,Satis,E_Alis,E_Satis,Tarih")] KurBilgisi kurBilgisi, DateTime date)
        {
            //string today = "http://www.tcmb.gov.tr/kurlar/today.xml";

            //date.Now.Day.ToString("dd");
            string gün = date.Day.ToString();
            if (Convert.ToInt32(gün) < 10) { gün = "0" + gün; }
            string ay = date.Month.ToString();
            if (Convert.ToInt32(ay) < 10) { ay = "0" + ay; }
            string yıl = date.Year.ToString();
            Debug.WriteLine("---------", gün);
            string today = "http://www.tcmb.gov.tr/kurlar/" + yıl + ay + "/" + gün + ay + yıl + ".xml";

            var xmlDoc = new XmlDocument();
            try { xmlDoc.Load(today); 

            DateTime tarih = Convert.ToDateTime(xmlDoc.SelectSingleNode("//Tarih_Date").Attributes["Tarih"].Value);
            kurBilgisi.Tarih = tarih;

            XmlNodeList xNodeList = xmlDoc.SelectNodes("Tarih_Date/child::node()");
            string cins = null, kod = null;
            string alis = null, satis = null, ealis = null, esatis = null;
            int sayac = 0;

            foreach (XmlNode xNode in xNodeList)
            {
                //KurBilgisi kurBilgisi = new KurBilgisi();
                if (xNode.Name == "Currency")
                {
                    kod = xNode.Attributes["Kod"].Value;
                    kurBilgisi.Kod = kod;
                    // System.Diagnostics.Debug.WriteLine("------------------------------", kod);
                    foreach (XmlNode xNode1 in xNode.ChildNodes)
                    {
                        if (xNode1.Name == "Isim")
                        {
                            cins = xNode1.InnerText;
                            kurBilgisi.Cins = cins;
                            //cins = "";
                            Debug.WriteLine("------------------------------", cins);
                        }
                        else if (xNode1.Name == "ForexBuying")
                        {
                            alis = xNode1.InnerText;
                            kurBilgisi.Alis = alis;
                        }
                        else if (xNode1.Name == "ForexSelling")
                        {
                            satis = xNode1.InnerText;
                            kurBilgisi.Satis = satis;
                        }
                        else if (xNode1.Name == "BanknoteBuying")
                        {
                            ealis = xNode1.InnerText;
                            kurBilgisi.E_Alis = ealis;
                        }
                        else if (xNode1.Name == "BanknoteSelling")
                        {
                            esatis = xNode1.InnerText;
                            kurBilgisi.E_Satis = esatis;
                        }

                    }
                    db.KurBilgisi.Add(kurBilgisi);
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        foreach (var validationErrors in dbEx.EntityValidationErrors)
                        {
                            foreach (var validationError in validationErrors.ValidationErrors)
                            {
                                Trace.TraceInformation("Class: {0}, Property: {1}, Error: {2}",
                                    validationErrors.Entry.Entity.GetType().FullName,
                                    validationError.PropertyName,
                                    validationError.ErrorMessage);
                            }
                        }
                        //db.KurBilgisi.Add(kurBilgisi);  
                    }

                }
                // KurBilgisi kurBilgisi2 = new KurBilgisi();
                // kurBilgisi = kurBilgisi2;


            }
        }
            catch
            {
                TempData["hata"] = "Bu tarihte geçerli kur bulunmamaktadır";
            }
        }
       
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
