﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace KurHesaplama.Models.ViewModel
{
    public class KurBilgileri
    {
            public int Id { get; set; }
        
            public string Kod { get; set; }
        
            public string Cins { get; set; }
        
            public string Alis { get; set; }
        
            public string Satis { get; set; }

            public string E_Alis { get; set; }
        
            public string E_Satis { get; set; }

            public DateTime tarih { get; set; }

            public string miktar { get; set; }

            public string sonuc { get; set; }

    }
        public class KurBilgileriContext : DbContext
        {
            public DbSet<KurBilgileri> KurBilgileri { get; set; }
            public System.Data.Entity.DbSet<KurHesaplama.Models.DB.KurBilgisi> KurBilgisis { get; set; }
        }
    }
